//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
    //Si quieres mandar un mensaje:
    //res.send("Hola mundo nodejs");
    //Si quieres llamar un archivo:
    res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/",function(req,res){
  res.send("Hemos recibido su petición POST.");
})

app.get("/clientes/:idcliente", function(req, res) {
    //1:
    //res.send("Aquí tienes al cliente número: " + req.params.idcliente);

    //2
    var mijson="{'idcliente':12345}";
    res.send(mijson);
})

//Práctica:

app.put("/",function(req,res){
  res.send("Hemos recibido su petición put cambiado.");
})

app.delete("/",function(req,res){
  res.send("Hemos recibido su petición DELETE.");
})

app.get("/v1/movimientos",function(req, res){
  res.sendFile(path.join(__dirname,'movimientos1.json'));
})

//Llamaremos al archivo movimientos2.json, asociándolo a la variable movimientosJSON con el comando "json"
var movimientosJSON = require('./movimientos2.json');
//Pintamos el archivo movimientos2.json
app.get("/v2/movimientos", function(req, res){
  res.json(movimientosJSON);
})

//Pasaremos un parámetro al get:
app.get("/v2/movimientos/:id",function(req, res){
  //El siguiente "console" mostrará la información en la consola del terminal.
  console.log(req.params.id);
  //Llamamos al parámetro id para la búsqueda...
  //Si queremos hacer la consulta en el browser o en el postman, ingresaremos la sgte url:
  //localhost:3000/v2/movimientos/2 -> En la que el "2" es el número de id...
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function(req, res){
  console.log(req.query);
  res.send("recibido");
  //Para llamar la consulta mediante el brower o postman, se escribirá el siguiente comando:
  //localhost:3000/v2/movimientosq?id=33&otrovalor=hola
})

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.post('/v2/movimientos', function(req, res){
  var nuevo = req.body;
  console.log("primer tramo");
  nuevo.id=movimientosJSON.length + 1;
  //Para insertar utilizamos el comando .push
  movimientosJSON.push(nuevo);
  res.send("Movimientos de alta");
})
